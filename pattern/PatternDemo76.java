package pattern;
/*
    A
   12
  ABC
 1234
ABCDE
*/
public class PatternDemo76 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int space=4;
		for(int i=0;i<line;i++) {
			int ch=1;
			char ch1='A';
			for(int k=0;k<space;k++) {
				System.out.print(" ");
				}
				for(int j=0;j<star;j++) {
					if(i%2==0)
						System.out.print(ch1++);
					else
						System.out.print(ch++);
				}
			System.out.println();
			space--;
			star++;
		}
	}

}

package pattern;
/*
ABCDE
 ABCD
  EAB
   CD
    E*/
public class PatternDemo63 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		int space=0;
		char ch='A';
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
				}
				for(int j=0;j<star;j++) {
					System.out.print(ch++);
					if(ch=='F')
						ch='A';
				}
			System.out.println();
			space++;
			star--;
		
		}
	}

}

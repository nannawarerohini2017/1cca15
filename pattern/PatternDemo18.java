package pattern;
/*
A 
A B 
A B C 
A B C D 
A B C D E 
 */
public class PatternDemo18 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		for(int i=0;i<line;i++) {
			char ch1='A';
			for(int j=0;j<star;j++) {
				System.out.print(ch1+" ");
				ch1++;
			}
			System.out.println();
			star++;
		}
	}

}

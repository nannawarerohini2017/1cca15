package pattern;
/*
1 6 11 16 21 26 
2 7 12 17 22 27 
3 8 13 18 23 28 
4 9 14 19 24 29 
5 10 15 20 25 30
 */
public class PatternDemo11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		int ch1=1;
		for(int i=0;i<line;i++) {
			int ch2=ch1;
			System.out.print(ch2);
			System.out.print(" ");
			for(int j=0;j<star;j++) {	
				System.out.print(ch2+=5);
				System.out.print(" ");
			}
			System.out.println();
			ch1++;
		}
	}

}

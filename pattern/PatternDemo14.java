package pattern;
/*
A
BB
CCC
DDDD
EEEEE
*/
public class PatternDemo14 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		char ch1='A';
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
				System.out.print(ch1);
			}
			System.out.println();
			star++;
			ch1++;
		}
	}

}

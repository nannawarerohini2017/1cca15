package pattern;
/*
12345
 ****
  123
   **
    1
*/
public class PatternDemo65 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		int space=0;
		for(int i=0;i<line;i++) {
			int ch1=1;
			for(int k=0;k<space;k++) {
				System.out.print(" ");
				}
				for(int j=0;j<star;j++) {
					if(i%2==0)
					System.out.print(ch1++);
					else
						System.out.print("*");
				}
			System.out.println();
			space++;
			star--;
			
		}
	}

}

package pattern;
/*
1
2 4
3 6 9
4 8 12 16
5 10 15 20 25
 */
public class PatternDemo15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int ch1=1;
		for(int i=0;i<line;i++) {
			int ch2=1;
			for(int j=0;j<star;j++) {
				System.out.print(" "+ch1*ch2+" ");
				ch2++;
			}
			System.out.println();
			star++;
			ch1++;
		}
	}
}

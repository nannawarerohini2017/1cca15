package pattern;
/*
AAAAA
 1111
  BBB
   22
    C
*/
public class PattternDemo68 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		int space=0;
		char ch='A';
		int num=1;
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
				}
				for(int j=0;j<star;j++) {
					if(i%2==0) {
					    System.out.print(ch);
					}
					else {
						System.out.print(num);
						}
				}
			System.out.println();
			space++;
			star--;
			if(i%2==0)
				ch++;
			else
				num++;
		}
	}

}

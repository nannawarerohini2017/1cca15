package pattern;
/*
 * 
 * 1
 * 1 * 
 * 1 * 3
 * 1 * 3 * 
 * 1 * 3
 * 1 * 
 * 1
 * 
 */
public class PatternDemo29 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=9;
		int star=1;
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
				if(j%2==0)
					System.out.print(" * ");
				else
					System.out.print(j);
			}
			System.out.println();
			if(i<4)	
			   star++;
			else
				star--;
		}
	}

}

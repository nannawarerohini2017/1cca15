package pattern;
/*
12345
23455
34555
45555
55555
*/
public class PatternDemo86 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		int ch=1;
		for(int i=0;i<line;i++) {
			int ch1=ch;
			for(int j=0;j<star;j++) {
				System.out.print(ch1++);
				if(ch1>5)
					ch1=5;	
			}
			System.out.println();
			ch++;
		}
	}

}

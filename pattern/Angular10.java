package pattern;
/*
   1 
  2 3 
 4 5 6 
7 8 9 10 
*/
public class Angular10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=4;
		int star=1;
		int space=3;
		int ch=1;
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
			}
			for(int j=0;j<star;j++) {
				System.out.print(ch+++" ");
				
			}
			System.out.println();
			star++;
			space--;
		}
	}

}

package pattern;
/*
 * 
 *  * 
 *  *  * 
 *  *  *  * 
 *  *  *  *  * 
 */
public class PatternDemo12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
					System.out.print(" * ");
			}
			System.out.println();
			star++;
		}
	}

}

package pattern;
/*
 A
 C C
 E E E
 G G
 I 
 */
public class PatternDemo28 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		char ch1='A';
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
				System.out.print(ch1);
			}
			System.out.println();
			ch1++;
			ch1++;
			if(i<2)	
			   star++;
			else
				star--;
		}
	}
}

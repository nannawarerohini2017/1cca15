package pattern;
/*
*
 *
  *
   *
  *
 *
*/
public class Diamond6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=7;
		int space=0;
		int star=1;
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
			}
			for(int j=0;j<star;j++) {
				System.out.print("* ");
			}
			System.out.println();
			if(i<=2) {
				space++;
			}
			else {
				space--;
			}
		}
	}

}
/*
int line=7
int star=1
for(int i=0;i<line;i++) {
for(int j=0;j<star;j++) {
if(j==star-1)
	System.out.print("*");
	else
	System.out.print(" ");
}
System.out.println();
if(i<=2)
	star++;
else
	star--;
}
*/
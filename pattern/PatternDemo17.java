package pattern;
/*
 1 
1 2 
1 2 3 
1 2 3 4 
1 2 3 4 5 
 */
public class PatternDemo17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		for(int i=0;i<line;i++) {
			int ch1=1;
			for(int j=0;j<star;j++) {
				System.out.print(ch1+" ");
				ch1++;
			}
			System.out.println();
			star++;
		}
	}

}

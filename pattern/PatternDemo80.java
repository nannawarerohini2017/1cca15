package pattern;
/*
    1
   23
  412
 3412
  341
   23
    4
*/
public class PatternDemo80 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=7;
		int star=1;
		int space=4;
		int ch1=1;
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
			}
			for(int j=0;j<star;j++) {
					System.out.print(ch1++);
					if(ch1==5)
						ch1=1;
			}
			System.out.println();
			if(i<3) {	
			   star++;
			space--;
			}
			else {
				star--;
			space++;
			}
		}
	}

}

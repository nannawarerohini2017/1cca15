package pattern;
/*
 *****
 ****
 ***
 **
 *
 **
 ***
 ****
 *****
 */
public class PatternDemo44 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=9;
		int star=5;
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
				System.out.print(" *");
			}
			System.out.println();
			if(i<4)	
			   star--;
			else
				star++;
		}
	}

}

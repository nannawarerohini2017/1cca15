package pattern;
/*
    A
   AB
  ABC
   AB
    A
*/
public class PatternDemo78 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int space=4;
		for(int i=0;i<line;i++) {
			char ch1='A';
			for(int k=0;k<space;k++) {
				System.out.print(" ");
			}
			for(int j=0;j<star;j++) {
					System.out.print(ch1++);
			}
			System.out.println();
			if(i<2) {	
			   star++;
			space--;
			}
			else {
				star--;
			space++;
			}
		}
	}

}

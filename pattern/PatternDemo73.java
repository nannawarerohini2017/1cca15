package pattern;
/*
    A
   **
  BBB
 ****
CCCCC
*/
public class PatternDemo73 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int space=4;
		char ch='A';
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
				}
				for(int j=0;j<star;j++) {
					if(i%2==0)
						System.out.print(ch);
					else
						System.out.print("*");
				}
			System.out.println();
			space--;
			star++;
			if(i%2==0)
			   ch++;
		}
	}

}

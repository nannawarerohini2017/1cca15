package pattern;
/*
1
23
412
3412
34123
*/
public class PatternDemo16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int ch1=1;
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
				System.out.print(ch1++);
				if(ch1>4) {
					ch1=1;
				}
			}
			System.out.println();
			star++;
		}
	}

}

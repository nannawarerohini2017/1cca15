package pattern;
/*
ABCDE
1234
ABC
12
A*/
public class PatternDemo71 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		
		for(int i=0;i<line;i++) {
			int ch=1;
			char ch1='A';
			for(int j=0;j<star;j++) {
				if(i%2==0)
				System.out.print(ch1++);
				else
					System.out.print(ch++);
			}
			System.out.println();
			star--;
		}
	}

}

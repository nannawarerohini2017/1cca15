package pattern;
/*
  1
 *2*
**3**/
public class Diamond5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=3;
		int space=2;
		int star=1;
		int ch=1;
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
			}
			for(int j=0;j<star;j++) {
				if(j==i) {
				System.out.print(ch);
				ch++;
				}
				else
					System.out.print("*");
			}
			System.out.println();
			if(i<=1) {
				star+=2;
				space--;
			}
		}
	}

}

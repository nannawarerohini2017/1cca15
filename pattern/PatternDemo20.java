package pattern;
/*
12345
1234
123
12
1       
*/
public class PatternDemo20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		for(int i=0;i<line;i++) {
			int ch=1;
			for(int j=0;j<star;j++) {
				System.out.print(ch);
				ch++;
			}
			System.out.println();
			star--;
		}
	}

}

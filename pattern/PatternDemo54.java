package pattern;/*
ABCDE
ABCD
ABC
AB
A*/
public class PatternDemo54 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		for(int i=0;i<line;i++) {
			char ch='A';
			for(int j=0;j<star;j++) {
				System.out.print(ch);
				ch++;
			}
			System.out.println();
			star--;
		}
	}

}

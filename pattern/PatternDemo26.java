package pattern;
/*
A
AB
ABC
AB
A
 */
public class PatternDemo26 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		for(int i=0;i<line;i++) {
			char ch1='A';
			for(int j=0;j<star;j++) {
					System.out.print(ch1++);
			}
			System.out.println();
			if(i<2)	
			   star++;
			else
				star--;
		}
	}

}

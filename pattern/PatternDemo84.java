package pattern;
/*
3
23
123
0123
123
23
3*/
public class PatternDemo84 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=7;
		int star=1;
		int ch=3;
		for(int i=0;i<line;i++) {
			int ch1=ch;
			for(int j=0;j<star;j++) {
				System.out.print(ch1++);
			}
			System.out.println();
			if(i<3) {
			star++;
			ch--;
			}
			else {
				star--;
				ch++;
			}
		}
	}

}

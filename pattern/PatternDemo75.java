package pattern;
/*
    A
   11
  BBB
 2222
CCCCC*/
public class PatternDemo75 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int space=4;
		int ch=1;
		char ch1='A';
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
				}
				for(int j=0;j<star;j++) {
					if(i%2==0)
						System.out.print(ch1);
					else
						System.out.print(ch);
				}
			System.out.println();
			space--;
			star++;
			if(i%2==0)
			   ch1++;
			else
				ch++;
		}
	}

}

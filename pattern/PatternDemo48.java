package pattern;
/*
    1
   12
  123
 1234
12345
 */
public class PatternDemo48 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int space=4;
		for(int i=0;i<line;i++) {
			int ch=1;
			for(int k=0;k<space;k++) {
				System.out.print(" ");
				}
				for(int j=0;j<star;j++) {
					System.out.print(ch++);
				}
			System.out.println();
			space--;
			star++;
			ch++;
		}
	}

}

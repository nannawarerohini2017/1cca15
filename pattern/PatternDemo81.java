package pattern;
/*
1 
2 3 
4 5 6 
7 8 9 10 
11 12 13 14 15 */
public class PatternDemo81 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int ch1=1;
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
					System.out.print(ch1+" ");
					ch1++;
			}
			System.out.println();	
			   star++;
		}
	}

}

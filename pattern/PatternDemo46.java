package pattern;
/*
    1
   22
  333
 4444
55555
*/
public class PatternDemo46 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int space=4;
		int ch=1;
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
				}
				for(int j=0;j<star;j++) {
					System.out.print(ch);
				}
			System.out.println();
			space--;
			star++;
			ch++;
		}
	}

}

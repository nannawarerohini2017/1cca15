package pattern;
/*
ABCDE
****
ABC
**
A*/
public class PatternDemo69 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		for(int i=0;i<line;i++) {
			char ch='A';
			for(int j=0;j<star;j++) {
				if(i%2==0)
				System.out.print(ch++);
				else
					System.out.print("*");
			}
			System.out.println();
			star--;
		}
	}

}

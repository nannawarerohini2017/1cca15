package pattern;
/*
**
111
****
22222
******
3333333*/
public class Angular6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=6;
		int star=2;
		int ch=1;
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
				if(i%2==0)
				System.out.print("*");
				else
					System.out.print(ch);
			}
			System.out.println();
			if(i%2!=0)
				ch++;
			star++;
		}
	}

}

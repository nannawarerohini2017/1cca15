package pattern;
/*
12345
23451
34512
45123
51234*/
public class Angular1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		int ch=1;
		for(int i=0;i<line;i++) {
			int ch1=ch;
			for(int j=0;j<star;j++) {
				System.out.print(ch1++);
				if(ch1>5)
					ch1=1;
			}
			System.out.println();
			ch++;
		}
	}

}

package pattern;
/*
     A
    BC
   DEA
  BCDE
 ABCDE
  ABCD
   EAB
    CD
     E
*/
public class PatternDemo79 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=9;
		int star=1;
		int space=5;
		char ch1='A';
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
			}
			for(int j=0;j<star;j++) {
					System.out.print(ch1++);
					if(ch1=='F')
						ch1='A';
			}
			System.out.println();
			if(i<4) {	
			   star++;
			space--;
			}
			else {
				star--;
			space++;
			}
		}
	}

}

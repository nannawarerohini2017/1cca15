package pattern;
/*
12345
 1234
  512
   34
    5*/
public class PatternDemo62 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		int space=0;
		int ch=1;
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
				}
				for(int j=0;j<star;j++) {
					System.out.print(ch++);
					if(ch==6)
						ch=1;
				}
			System.out.println();
			space++;
			star--;
		}
	}

}

package pattern;
/*
   * 
  * * 
 * * * 
  * * 
   * 
 */
public class Pyramid6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int space=3;
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
			}
			for(int j=0;j<star;j++) {
					System.out.print("*"+" ");
			}
			System.out.println();
			if(i<2) {	
			   star++;
			space--;
			}
			else {
				star--;
			space++;
			}
	}

	}
	
}

package pattern;
/*
A 
BB
CCC
DDDD
EEE
FF
G
 */
public class PatternDemo27 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=7;
		int star=1;
		char ch1='A';
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
					System.out.print(ch1);
			}
			System.out.println();
			ch1++;
			if(i<3)	
			   star++;
			else
				star--;
		}
	}

}

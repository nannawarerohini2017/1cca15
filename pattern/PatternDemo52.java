package pattern;
/*
    1
   23
  412
 3412
34123*/
public class PatternDemo52 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int space=4;
		int ch=1;
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
				}
				for(int j=0;j<star;j++) {
					System.out.print(ch++);
					if(ch==5) {
						ch=1;
					}
				}
			System.out.println();
			space--;
			star++;
		}
	}

}

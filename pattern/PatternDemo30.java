package pattern;
/*
 A
 B C
 D E A
 B C D E
 A B C D E
 A B C D
 E A B 
 C D
 E
 */
public class PatternDemo30 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=9;
		int star=1;
		char ch='A';
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
				System.out.print(ch);
				ch++;
				if(ch>'E') {
					ch='A';
				}
			}
			System.out.println();
			if(i<4)	
			   star++;
			else
				star--;
		}
	}

}

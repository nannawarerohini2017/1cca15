package pattern;
/*
    A 
   B C 
  D E F 
 G H I J 
*/
public class Pyramid3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=4;
		int star=1;
		int space=4;
		char ch='A';
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
			}
			for(int j=0;j<star;j++) {
				System.out.print(ch+" ");
				ch++;
			}
			System.out.println();
			space--;
			star++;
		}

	}

}

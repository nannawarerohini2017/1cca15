package pattern;
/*
 A  A  A  A  A  A  A 
 B  B  B  B  B 
 C  C  C 
 D 
 */
public class PatternDemo21 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=4;
		int star=7;
		char ch='A';
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
				System.out.print(" "+ch+" ");
			}
			System.out.println();
			star-=2;
			ch++;
		}
	}

}

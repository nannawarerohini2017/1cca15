package pattern;
/******
*ABC*
*DEF*
*GHI*
*****/
public class PatternDemo42 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int rows=5;
		int cols=5;
		char ch='A';
		for(int i=0;i<rows;i++) {
			for(int j=0;j<cols;j++) {
				if(i==0 || j==0 || i==4 || j==4) {
					System.out.print("*");
				}
				else {
					System.out.print(ch);
					ch++;
				}
			}
			System.out.println();
		}
	}

}

package pattern;
/*
    * 
   * * 
  * * * 
 * * * * 
*/
public class Pyramid1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=4;
		int star=1;
		int space=4;
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
			}
			for(int j=0;j<star;j++) {
				System.out.print("*"+" ");
			}
			System.out.println();
			space--;
			star++;
		}
	}

}

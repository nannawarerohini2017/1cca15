package pattern;
/*
 12345
  1234
   123
    12
     1
*/
public class PatternDemo61 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		int space=0;
		for(int i=0;i<line;i++) {
			int ch=1;
			for(int k=0;k<space;k++) {
				System.out.print(" ");
				}
				for(int j=0;j<star;j++) {
					System.out.print(ch++);
				}
			System.out.println();
			space++;
			star--;
			ch++;
		}
	}

}

package pattern;
/*
1
22
333
4444
55555
*/
public class PatternDemo13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int ch1=1;
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
				System.out.print(ch1);
			}
			System.out.println();
			star++;
			ch1++;
		}
	}

}

package pattern;
/*
  A
 A A
A   A
 A A
  A*/
public class Diamond4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int space=2;
		int star=1;
		char ch='A';
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
			}
			for(int j=0;j<star;j++) {
				if(j==0 || j==star-1)
				System.out.print(ch);
				else
					System.out.print(" ");
			}
			System.out.println();
			if(i<=1) {
				star+=2;
				space--;
			}
			else {
				star-=2;
				space++;
			}
		}
	}

}

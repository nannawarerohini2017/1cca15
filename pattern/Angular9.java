package pattern;
/*
1 2 
2 4 8 
3 6 12 24 
4 8 16 32 64 
5 10 20 40 80 160 
*/
public class Angular9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=2;
		int ch=1;
		for(int i=0;i<line;i++) {
			int ch1=ch;
			for(int j=0;j<star;j++) {
				System.out.print(ch1+" ");
				ch1+=ch1;
			}
			System.out.println();
			star++;
			ch++;
		}
	}

}

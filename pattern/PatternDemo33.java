package pattern;
/*
1 2 3 4 5
A B C D E
1 2 3 4 5
A B C D E
1 2 3 4 5
*/
public class PatternDemo33 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		for(int i=0;i<line;i++) {
			int ch=1;
			char ch1='A';
			for(int j=0;j<star;j++) {
				if(i%2==0) {
				System.out.print(" "+ch);
				ch++;
				}
				else {
					System.out.print(" "+ch1);
					ch1++;
				}
			}
			System.out.println();
		}
	}

}

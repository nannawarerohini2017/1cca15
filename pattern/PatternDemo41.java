package pattern;
/*
*****
*ABC*
*ABC*
*ABC*
*****/
public class PatternDemo41 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int rows=5;
		int cols=5;
		
		for(int i=0;i<rows;i++) {
			char ch='A';
			for(int j=0;j<cols;j++) {
				if(i==0 || j==0 || i==4 || j==4) {
					System.out.print("*");
				}
				else {
					System.out.print(ch);
					ch++;
				}
			}
			System.out.println();
		}
	}

}

package pattern;
/*
0 
0 1 1 
0 1 1 2 3 
0 1 1 2 3 5 8 
0 1 1 2 3 5 8 13 21 */
public class PatternDemo88 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		for(int i=0;i<line;i++) {
			int ch=0,ch1=1;
			for(int j=0;j<star;j++) {
				System.out.print(ch+ " ");
				int a=ch+ch1;
				ch=ch1;
				ch1=a;
			}
			System.out.println();
			star+=2;
		}
	}

}

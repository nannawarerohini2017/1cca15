package pattern;
/*
    1
   12
  123
   12
    1
*/
public class PatternDemo77 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=1;
		int space=4;
		for(int i=0;i<line;i++) {
			int ch1=1;
			for(int k=0;k<space;k++) {
				System.out.print(" ");
			}
			for(int j=0;j<star;j++) {
					System.out.print(ch1++);
			}
			System.out.println();
			if(i<2) {	
			   star++;
			space--;
			}
			else {
				star--;
			space++;
			}
		}
	}

}

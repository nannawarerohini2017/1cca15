package pattern;/*
*****
*123*
*123*
*123*
*****/
public class PatternDemo39 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				int rows=5;
				int cols=5;
				
				for(int i=0;i<rows;i++) {
					int ch=1;
					for(int j=0;j<cols;j++) {
						if(i==0 || j==0 || i==4 || j==4) {
							System.out.print("*");
						}
						else {
							System.out.print(ch);
							ch++;
						}
					}
					System.out.println();
				}
			}
}


package pattern;
/*
AAAAA
1111
BBB
22
C
*/
public class PatternDemo72 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		int star=5;
		int ch=1;
		char ch1='A';
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
				if(i%2==0)
				System.out.print(ch1);
				else
					System.out.print(ch);
			}
			System.out.println();
			star--;
			if(i%2==0)
				ch1++;
			else
				ch++;
		}
	}

}

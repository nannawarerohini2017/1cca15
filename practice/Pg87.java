package practice;
/*
A   A
 A A 
  A  
 A A 
A   A
 */
public class Pg87 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=5;
		char ch='A';
		int star=5;
		for(int i=0;i<line;i++) {
			for(int j=0;j<star;j++) {
				if(i==j || i+j==4)
					System.out.print(ch);
				else
					System.out.print(" ");
			}
			System.out.println();
		}
	}

}

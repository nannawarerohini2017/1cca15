package practice;
/*
    1 
   2 2 
  3 3 3 
 4 4 4 4 
5 5 5 5 5 
 4 4 4 4 
  3 3 3 
   2 2 
    1 
 */
public class Pg51 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=9;
		int star=1;
		int ch=1;
		int space=4;
		for(int i=0;i<line;i++) {
			for(int k=0;k<space;k++) {
				System.out.print(" ");
			}
			for(int j=0;j<star;j++) {
				System.out.print(ch+" ");
			}
			System.out.println();
			if(i<4) {
			ch++;
			space--;
			star++;
			}
			else {
				ch--;
				space++;
				star--;
			}
		}
	}

}

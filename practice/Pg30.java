package practice;
/*
FEDCBA
EDCBA
DCBA
CBA
BA
A
A
BA
CBA
DCBA
EDCBA
FEDCBA

 */
public class Pg30 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int line=11;
		int star=6;
		char ch1='F';
		for(int i=0;i<line;i++) {
			char ch=ch1;
			for(int j=0;j<star;j++) {
				if(i<5)
				System.out.print(ch--);
				else {
					System.out.print(ch);
					--ch;
				}
				
			}
			System.out.println();
			if(i<5) {
				ch1--;
			    star--;
			}
			else if(i>5){
				++ch1;
			    star++;
			}
		}
	}

}

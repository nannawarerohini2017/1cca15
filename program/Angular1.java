package program;

public class Angular1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr1= {1,3,5,7,9};
		int[] arr2= {2,4,6,8,10};
		int n1=arr1.length;
		int n2=arr2.length;
		int[] arr3=new int[n1+n2];
		int ch1=0;
		int ch2=0;
		for(int i=0;i<n1+n2;i++) {
			if(i%2==0 && ch1<=n1)
				arr3[i]=arr1[ch1++];
			else if(i%2!=0 && ch2<=n2)
				arr3[i]=arr2[ch2++];
		}
		for(int a:arr3)
		 System.out.print(a+"   ");
	}

}

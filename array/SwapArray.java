package array;

import java.util.Arrays;

public class SwapArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a1= {1,2,3,4};
		int[] a2= {5,6,7,8};
		int[] a3=new int[a1.length+a2.length];
		int index=0;
		for(int a:a1) {
			a3[index]=a;
			index+=2;
		}
		index=1;
		for(int a:a2) {
			a3[index]=a;
			index+=2;
		}
		System.out.println(Arrays.toString(a3));
	}

}

package array;

public class ReverseArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr= {1,2,3,4,5,6};
		for(int i=0;i<arr.length;i++) {
			int temp=0;
			temp=arr[i];
			arr[i]=arr[arr.length-temp];
			arr[arr.length-temp]=temp;
			System.out.print(arr[i]);
		}
		
	}
}

package array;

import java.util.Arrays;

public class LargestNo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr=Array3.randomArr(6);
		System.out.println(Arrays.toString(arr));
		int max=arr[0];
		for(int i=0;i<arr.length;i++) {
			if(arr[i]>max)
				max=arr[i];
		}
		System.out.print(max);
	}
}
/*
 for(int i:arr1){
 if(i>max){
 max=i;}
 S.o.p(max);
 */

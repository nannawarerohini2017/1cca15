package array;

public class Array1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    int[] arr1= {1,2,3,4,5};
	    for(int a:arr1) {
	    	System.out.print(a+" ");
	    }
	    String[] arr= {"Abc","Pqr","Xyz"};
	    for(String b:arr)
	    	System.out.print(b+" ");
	    int[] arr2= {2,4,6,8,10,12,14,16,18,20};
	    int sum=0;
	    for(int c:arr2) {
	    	sum=sum+c;
	    }
	    System.out.println("Sum of array element:"+sum);
	    int avg=sum/arr2.length;
	    System.out.println("Average:"+avg);
	}

}

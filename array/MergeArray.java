package array;

import java.util.Arrays;
//Arrays.toString function is used for display in above format.[1, 2, 3, 4, 5, 6, 7, 8, 9]
public class MergeArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a1= {1,2,3,4};
		int[] a2= {5,6,7,8,9};
		int[] a3=new int[a1.length+a2.length];
		int index=0;
		for(int i:a1) {
			a3[index]=i;
			index++;
		}
		for(int j:a2) {
			a3[index]=j;
			index++;
		}
		/*for(int i=0;i<a3.length;i++)
			System.out.print(a3[i]+" ");*/
		System.out.println(Arrays.toString(a3));
		
	}

}
